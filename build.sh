#!/bin/sh

OUTPUT_DIR="$(pwd)"
TMP_DIR="$(mktemp -d /tmp/journal_XXXX)"
NAME_CONTENT="$TMP_DIR/content.tex"
NAME_HEADER="$TMP_DIR/header.tex"
NAME_EVENTS="$TMP_DIR/events.tex"

if [ $# -ne 0 ]; then
	cd "$1" || (echo "Can not cd to $1"; exit 1)
fi

echo "" > "$NAME_CONTENT"

DATE="$(grep "date" ./*.journal | cut -d ':' -f 2)"
NUM="$(grep "numero" ./*.journal | cut -d ':' -f 2)"
AUTHORS="$(grep "auteurs" ./*.journal | cut -d ':' -f 2)"


{
echo "\\SetAuthors{$AUTHORS}" ;
echo "\\date{$DATE}";
echo "\\currentissue{$NUM}"
} > "$NAME_HEADER"


for i in *.article; do 
	TITLE="$(head -n1 "$i" | cut -d ':' -f 2 | sed 's/^[ \t]\?//')"
	BODY="$(tail -n +4 "$i" | sed 's/\ ?/\~?/g' | sed 's/\ !/\~!/g' | sed 's/\ :/\~:/g' | sed 's/\ ;/\~;/g' )"
	{
		echo "\\begin{article}{$TITLE}";
		echo "$BODY"
		echo "\\end{article}"
	} >> "$NAME_CONTENT"
done;

perl -ne "my @words = split / :/ ; print \"\\\\item [\$words[0] :]\$words[1]\"" ./*.calendrier > "$NAME_EVENTS"

cd "$TMP_DIR" || (echo "Can not cd to $TMP_DIR"; exit 2);

cat > bulletin.tex <<__BULLETIN_EOF
\\documentclass{article}

\\usepackage[utf8]{inputenc}
\\usepackage{bulletin}
\\usepackage{multicol}
\\usepackage{etoolbox}

\\input{header.tex}

\\SetPaperName{Bulletin du \\emph{p'Tit véLo}}
\\SetHeaderName{Bulletin du \\emph{p'Tit véLo}}
\\SetPaperLocation{\\'Edition du p'Tit véLo}
\\SetPaperSlogan{Vélorution~!}
\\SetPaperPrice{}
\\SetFrequency{Distribué quand il est prêt} 


\\newtoggle{firstarticle}

\\newenvironment{article}[1]
{
\\iftoggle{firstarticle}{\\togglefalse{firstarticle}}{}
\\paragraph*{#1~:}
}
{\\closearticle}

\\begin{document}
\\maketitle
\\begin{multicols}{3}

	\\input{content.tex}

\\end{multicols}
\\vfill
\\fbox{
	\\begin{minipage}{\\textwidth}
	\\textbf{Évènements à venir (pour participer : {\\tt https://pad.gresille.org/p/ptit-velo-anim}) :}
	\\begin{multicols}{2}
		\\begin{description}
				\\input{events.tex}
		\\end{description}
	\\end{multicols}
	\\end{minipage}
}
\\end{document}
__BULLETIN_EOF

cat > bulletin.sty <<__BULLETIN_STY_EOF
%%%	Martin Vassor
%%%	January 30, 2019
%%%	Heavily based on newspaper package
%%%	
%%%	======
%%%
%%%	Matthew Allen
%%%	January 14, 2007
%%%
%%%	This package provides a newspaper style heading
%%%	for the standard Article class.  The default plain
%%%	page style is redefined to accomodate headings 
%%%	at the top of all subsequent pages.
%%%
%%%	a good idea to use with this package is
%%%	the multicols package and the picinpar package
%%%
%%%	

%******* 	Identification   *****
\\ProvidesPackage{bulletin}
\\NeedsTeXFormat{LaTeX2e}

%******* 	Declaration of options *****

% no options at this time

%******* 	Execution of options   *****

%****** 	Package Loading   *****
\\RequirePackage{helvet}  % used for the paper title font

%******		main code *****

%%%%%%%%%%% Define Text Dimensions  %%%%%%%

\\setlength\\topmargin{-48pt} 		% article default = -58pt
\\setlength\\headheight{0pt}  		% article default = 12pt
\\setlength\\headsep{34pt}		% article default = 25pt
\\setlength\\marginparwidth{-20pt}	% article default = 121pt
\\setlength\\textwidth{7.0in}		% article default = 418pt
\\setlength\\textheight{10in}		% article default = 296pt
\\setlength\\oddsidemargin{-30pt}

%%%% counters for volume and number %%%%

\\newcounter{issue}
\\newcommand\\currentissue[1]{\\setcounter{issue}{#1}}

%%%% set internal variables %%%%

\\def\\@papername{Committee Times:}
\\def\\@headername{Committee Times}   % because of the yfonts you may need both papername and headername
\\def\\@paperlocation{Washington DC}
\\def\\@paperslogan{``All the News I Feel Like Printing.''}
\\def\\@paperprice{Zero Dollars}
\\def\\@frequency{Published every mondays}
\\def\\@authors{}

\\newcommand\\SetPaperName[1]{%
	\\def\\@papername{#1}}
\\newcommand\\SetHeaderName[1]{%
	\\def\\@headername{#1}}
\\newcommand\\SetPaperLocation[1]{%
	\\def\\@paperlocation{#1}}
\\newcommand\\SetPaperSlogan[1]{%
	\\def\\@paperslogan{#1}}
\\newcommand\\SetPaperPrice[1]{%
	\\def\\@paperprice{#1}}
\\newcommand\\SetFrequency[1]{
	\\def\\@frequency{#1}}
\\newcommand\\SetAuthors[1]{
	\\def\\@authors{#1}}


%%%%%%%%%%% Redefine \\maketitle     %%%%%%%

\\renewcommand{\\maketitle}{\\thispagestyle{empty}
\\vspace*{-40pt}
\\begin{center}
	{\\fontfamily{phv}\\selectfont{\\huge \\@papername}}\\\\
	\\vspace{0.3in}%
	\\textbf{\\footnotesize \\@paperlocation}\\hfill \\textbf{\\footnotesize \\@frequency}\\\\
	\\rule[0pt]{\\textwidth}{0.5pt}\\\\
	{\\small No.~\\arabic{issue}} \\hfill \\MakeUppercase{\\small\\it\\@date}\\\\
	\\rule[6pt]{\\textwidth}{1.2pt}
\\end{center}
\\pagestyle{plain}
}




\\renewcommand{\\ps@empty}{%
		\\renewcommand\\@oddfoot{
			{\\footnotesize Rédaction : \\@authors.\\hfill{\\tt https://pad.gresille.org/p/bulletin}}
			}
		\\let\\@evenfoot\\@oddfoot						% empty verso footer
		\\renewcommand\\@evenhead {}%
		\\let\\@oddhead\\@evenhead
		}

%%%%%%%   redefine plain page style  %%%%%%%
\\renewcommand{\\ps@plain}{%
		\\renewcommand\\@oddfoot{}%					% empty recto footer
		\\let\\@evenfoot\\@oddfoot						% empty verso footer
		\\renewcommand\\@evenhead
			{\\parbox{\\textwidth}{\\vspace*{4pt}
			{\\small No.\\arabic{issue}}\\hfill\\normalfont\\textbf{\\@headername}\\quad\\MakeUppercase{\\it\\@date}\\hfill\\textrm{\\thepage}\\\\
			\\rule{\\textwidth}{0.5pt}
			\\vspace*{12pt}}}%
		\\let\\@oddhead\\@evenhead
		}
		

%%%%%%%%%%%  Headline (with byline) command  %%%%%%%%%

\\newcommand\\headline[1]{\\begin{center} #1\\\\ %
			\\rule[3pt]{0.4\\hsize}{0.5pt}\\\\ \\end{center} \\par}
\\newcommand\\byline[2]{\\begin{center} #1 \\\\%
			{\\footnotesize\\bf By \\MakeUppercase{#2}} \\\\ %
			\\rule[3pt]{0.4\\hsize}{0.5pt}\\\\ \\end{center} \\par}
\\newcommand\\closearticle{{\\begin{center}\\rule[6pt]{\\hsize}{1pt}\\vspace*{-16pt}
			\\rule{\\hsize}{0.5pt}\\end{center}\\vspace*{-10pt}}}



%%%%%%%%%%%%%%%%%%%% End of Package   %%%%%%%%%%%%%%%

__BULLETIN_STY_EOF

pdflatex bulletin.tex | grep "Output written" || (echo "LaTeX failed"; exit 3)

pdfnup --frame true --suffix "a3" bulletin.pdf bulletin.pdf 
cp bulletin-a3.pdf bulletin.pdf "$OUTPUT_DIR"

cd "$OUTPUT_DIR" || (echo "Can not cd to $OUTPUT_DIR"; exit 2);


rm -r "$TMP_DIR"
